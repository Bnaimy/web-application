package com.websystique.springboot.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.websystique.springboot.model.User;
import com.websystique.springboot.service.UserService;

@RestController
public class RestApiController {
	@Autowired
	  UserService userService; //Service which will do all data retrieval/manipulation work
	  @RequestMapping(path="/client", method=RequestMethod.GET)
	  public List<User> getAllEmployees(){
		  Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>)    SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		  UserDetails userDetails =  (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		  if(authorities.contains(new SimpleGrantedAuthority("ROLE_USER"))) {
			  return Arrays.asList(userService.findByName(userDetails.getUsername()));
		  }
	    return userService.findUsers();
	  }
	    @RequestMapping(path="/client/{id}", method = RequestMethod.GET)
	  public User findById(@PathVariable("id") long id){
	    return userService.findById(id);
	  }
//		@GetMapping("/addabsence")
//		public ResponseEntity<Boolean> makeAbcent(@RequestParam("check") int[] ids ){
//			
//			int flag =0;
//			for(int i : ids) {
//				if(!userService.makeUserAbsent(i, true))
//					flag=1;
//			}
//			return ResponseEntity.ok(flag==1 ? false:true);
//
//			
//		}

}