package com.websystique.springboot.controller;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.websystique.springboot.model.User;
import com.websystique.springboot.repositories.AbsRepository;
import com.websystique.springboot.repositories.UserRepository;
import com.websystique.springboot.service.UserService;

@Controller
public class AppController {
	private final UserRepository userRepository;
	
	@Autowired
	AbsRepository absRepository;

    @Autowired
    public  AppController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
    @Autowired
    UserService userservice;
    
    @GetMapping("/admin")
    public String showform(User user) {
        return "add-user";
    }
    @GetMapping("/skip3")
    public String showform1(User user) {
        return "add-user";
    }
    @GetMapping(value="/skip")
    public String test(@Valid User user, BindingResult result, Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "admin";
    }
    @GetMapping("/skip2")
	public String test2(Model model){		
		 model.addAttribute("users", absRepository.findAll());
		 return "absenct";
	}
    @PostMapping("/adduser")
    public String addUser(@Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-user";
        }   
        userRepository.save(user);
        model.addAttribute("users", userRepository.findAll());
        return "admin";
    }
    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addAttribute("user", user);
        return "update-user";
    } 
    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            user.setId(id);
            return "update-user";
        }
        userRepository.save(user);
        model.addAttribute("users", userRepository.findAll());
        return "admin";
    }
    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        userRepository.delete(user);
        model.addAttribute("users", userRepository.findAll());
        return "admin";
    }
	 @RequestMapping(value="/")
	    public String home(){
	        return "home";
	    }
	    @RequestMapping(value="/login")
	    public String login(){
	        return "login";
	    }
	    @RequestMapping(path="/rec", method=RequestMethod.GET)
	    public ModelAndView getAllReclamations() {
	   	  ModelAndView model = new ModelAndView();
	   	  model.setViewName("/client");
	   	  return model;
	   	}
	    @RequestMapping(value="/403")
	    public String Error403(){
	        return "403";
	    }
	    
	    @GetMapping("/addabsence")
		public String makeAbcent(@RequestParam("check") int[] ids ,@RequestParam("period")String[] period,Model model){
			int flag =0;
			int count =0;
			for(int i : ids) {
				count=0;
				for(String str : period) {
					
					if(str!=null && !str.isEmpty() && !str.equals(" ")) {
						if(!userservice.makeUserAbsent(i, true,str)) {
							flag=1;
							
						}else {
							period[count]="";
							break;
						}
							
					}
					count++;
				}
				
			}
			
			 model.addAttribute("users", absRepository.findAll());
			 return "absenct";
		}
}
