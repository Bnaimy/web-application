/**
 * 
 */
package com.websystique.springboot.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Noeik
 *
 */

@Entity
@Table(name="user_absence")
public class Absence {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@NotEmpty
	@Column(name="name", nullable=false)
	public String name;

	@Column(name="filiere", nullable=false)
	public String filiere;

	@Column(name="nbr", nullable=false)
	public Integer nbr;
	
	@Column(name="absence_period")
	public String period;
	
	@Column(name="createddate")
	public Date date;
	
	
	
	
	public Integer getId() {
		return id;
	}




	public void setId(Integer id) {
		this.id = id;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getFiliere() {
		return filiere;
	}




	public void setFiliere(String filiere) {
		this.filiere = filiere;
	}




	public Integer getNbr() {
		return nbr;
	}




	public void setNbr(Integer nbr) {
		this.nbr = nbr;
	}
	
	




	public String getPeriod() {
		return period;
	}




	public void setPeriod(String period) {
		this.period = period;
	}
	
	




	public Date getDate() {
		return date;
	}




	public void setDate(Date date) {
		this.date = date;
	}

	public User toUser() {
		User user = new User();
		user.setfiliere(this.filiere);
		user.setname(this.name);
		user.setnbr(this.nbr);
		return user;
	}
}
