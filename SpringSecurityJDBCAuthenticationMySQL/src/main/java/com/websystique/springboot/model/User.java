package com.websystique.springboot.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotEmpty;

@SuppressWarnings("serial")
@Entity
@Table(name="APP_USER")
public class User implements Serializable{

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	public Long id;

	@NotEmpty
	@Column(name="name", nullable=false)
	public String name;

	@Column(name="filiere", nullable=false)
	public String filiere;

	@Column(name="nbr", nullable=false)
	public Integer nbr;
	
	@Transient
	public String period;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getname() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public String getfiliere() {
		return filiere;
	}

	public void setfiliere(String filiere) {
		this.filiere = filiere;
	}

	public Integer getnbr() {
		return nbr;
	}

	public void setnbr(Integer nbr) {
		this.nbr = nbr;
	}
	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public Absence toAbsence() {
		Absence abs = new Absence();
		abs.setFiliere(this.filiere);
		abs.setName(this.name);
		abs.setNbr(this.nbr+2);
		return abs;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", filiere=" + filiere
				+ ", nbr=" + nbr + "]";
	}
	
}
