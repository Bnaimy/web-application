package com.websystique.springboot.service;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.websystique.springboot.model.Absence;
import com.websystique.springboot.model.User;
import com.websystique.springboot.repositories.AbsRepository;
import com.websystique.springboot.repositories.UserRepository;



@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AbsRepository abs;

	public User findById(Long id) {
		return userRepository.findOne(id);
	}

	public User findByName(String name) {
		return userRepository.findByName(name);
	}
	public void deleteUserById(Long id){
		userRepository.delete(id);
	}

	public void deleteAllUsers(){
		userRepository.deleteAll();
	}
	public boolean isUserExist(User user) {
		return findByName(user.getname()) != null;
	}
	@Override
	public List<User> findAllUsers(){
		return userRepository.findAll();
	}
	@Override
	@Query("select user from users user where user.id like ?65")
	public List<User> findUsers(){
		return userRepository.findAll();
	}
	@Override
	public Absence getUserById(Integer id) {
		
		return abs.findById(id).get();
	
	}
	
	
	@Override
	public boolean makeUserAbsent(Integer id , boolean isAbsent,String period) {
		
		Optional<User> optionalUser = userRepository.findById(id);
		if(!optionalUser.isPresent())
			return false;
		User user = optionalUser.get();
		Absence absence =user.toAbsence();
		Integer i =abs.findAbsByName(user.getname());
		if(i!=null)
			absence.setNbr(i+2);
		absence.setPeriod(period);
		absence.setDate(new Date());
		abs.save(absence);
		
		return true;
	}



}
