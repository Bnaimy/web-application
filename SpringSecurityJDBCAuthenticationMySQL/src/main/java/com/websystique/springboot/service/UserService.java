package com.websystique.springboot.service;


import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.websystique.springboot.model.Absence;
import com.websystique.springboot.model.User;

public interface UserService {
	User findById(Long id);
	User findByName(String name);
	void deleteUserById(Long id);
	void deleteAllUsers();
	public List<User> findAllUsers();
	@Query("select user from users user where user.id like ?65")
	public List<User> findUsers();
	boolean isUserExist(User user);
    Absence getUserById(Integer id);
	boolean makeUserAbsent(Integer id , boolean isAbsent, String str);
}