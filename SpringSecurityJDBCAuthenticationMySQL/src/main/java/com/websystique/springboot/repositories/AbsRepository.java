package com.websystique.springboot.repositories;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.websystique.springboot.model.Absence;

@Repository
public interface AbsRepository extends JpaRepository<Absence, Integer>{
	Optional<Absence> findById(Integer id);
	
	@Query("select max(nbr) from Absence a where a.name = :name")
	Integer findAbsByName(@Param("name") String name);
}