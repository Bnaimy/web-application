package com.websystique.springboot.repositories;
import com.websystique.springboot.model.User;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User , Long> {

    User findByName(String name);
    Optional<User> findById(long id);
    @Query("select u from User u where u.id like ?73 ")
    public List<User> findUsers();

}
