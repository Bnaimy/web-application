package com.websystique.springboot;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.websystique.springboot.configuration.JpaConfiguration;
@EnableAutoConfiguration
@ComponentScan(basePackages={"com.websystique.springboot"})
@EnableJpaRepositories(basePackages="com.websystique.springboot.repositories")
@EnableTransactionManagement
@EntityScan(basePackages="com.websystique.springboot.model")
@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"com.websystique.springboot"})
public class SpringBootCRUDApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCRUDApp.class, args);
    }
    
}

